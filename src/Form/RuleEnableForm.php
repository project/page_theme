<?php

namespace Drupal\page_theme\Form;

use Drupal\Core\Entity\EntityConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;


/**
 * Provides the page theme rule enable form.
 */
class RuleEnableForm extends EntityConfirmFormBase {


  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    return $this->t('Are you sure you want to enable the page theme rule %label?', array('%label' => $this->entity->label()));
  }


  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    return new Url('entity.page_theme_rule.collection');
  }


  /**
   * {@inheritdoc}
   */
  public function getConfirmText() {
    return $this->t('Enable');
  }


  /**
   * {@inheritdoc}
   */
  public function getDescription() {
    return '';
  }


  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->entity->enable()->save();

    drupal_set_message($this->t('Rule %label has been enabled.', array('%label' => $this->entity->label())));
    $this->logger('page_theme')->notice('Rule %label has been enabled.', array('%label' => $this->entity->label(), 'link' => $this->entity->link($this->t('Edit'))));

    $form_state->setRedirect('entity.page_theme_rule.collection');
  }

}
