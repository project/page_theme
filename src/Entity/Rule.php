<?php

namespace Drupal\page_theme\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBase;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\page_theme\RuleInterface;


/**
 * Defines the page theme rule entity class.
 *
 * @ConfigEntityType(
 *   id = "page_theme_rule",
 *   label = @Translation("Page theme rule"),
 *   handlers = {
 *     "list_builder" = "Drupal\page_theme\RuleListBuilder",
 *     "form" = {
 *       "default" = "Drupal\page_theme\RuleForm",
 *       "delete" = "Drupal\Core\Entity\EntityDeleteForm",
 *       "enable" = "Drupal\page_theme\Form\RuleEnableForm",
 *       "disable" = "Drupal\page_theme\Form\RuleDisableForm",
 *     }
 *   },
 *   admin_permission = "administer themes",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "theme" = "theme",
 *     "status" = "status",
 *     "weight" = "weight",
 *   },
 *   links = {
 *     "collection" = "/admin/appearance/page-theme",
 *     "edit-form" = "/admin/appearance/page-theme/manage/{page_theme_rule}",
 *     "delete-form" = "/admin/appearance/page-theme/manage/{page_theme_rule}/delete",
 *     "enable" = "/admin/appearance/page-theme/manage/{page_theme_rule}/enable",
 *     "disable" = "/admin/appearance/page-theme/manage/{page_theme_rule}/disable",
 *   },
 *   config_prefix = "rule",
 *   config_export = {
 *     "id",
 *     "label",
 *     "theme",
 *     "weight",
 *   }
 * )
 */
class Rule extends ConfigEntityBase implements RuleInterface {


  /**
   * The machine name of this rule.
   *
   * @var string
   */
  protected $id;


  /**
   * The human-readable label of this rule.
   *
   * @var string
   */
  protected $label;


  /**
   * The theme of this rule.
   *
   * @var string
   */
  protected $theme;


  /**
   * The weight of this rule.
   *
   * @var int
   */
  protected $weight = 0;


  /**
   * {@inheritdoc}
   */
  public function getTheme() {
    return $this->theme;
  }


  /**
   * {@inheritdoc}
   */
  public function getWeight() {
    return $this->weight;
  }

}
