<?php

namespace Drupal\page_theme;

use Drupal\Core\Config\Entity\ConfigEntityInterface;


/**
 * Provides an interface defining a page theme rule entity.
 */
interface RuleInterface extends ConfigEntityInterface {


  /**
   * Returns the theme.
   *
   * @return string
   *   The theme of this rule.
   */
  public function getTheme();


  /**
   * Returns the weight.
   *
   * @return int
   *   The weight of this rule.
   */
  public function getWeight();

}
