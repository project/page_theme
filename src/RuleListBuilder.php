<?php

namespace Drupal\page_theme;

use Drupal\Core\Config\Entity\DraggableListBuilder;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Form\FormStateInterface;


/**
 * Defines a class to build a listing of page theme rule entities.
 *
 * @see \Drupal\page_theme\Entity\Rule
 */
class RuleListBuilder extends DraggableListBuilder {


  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'page_theme_admin_rules_form';
  }


  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['label'] = $this->t('Name');
    $header['theme'] = $this->t('Theme');
    $header['status'] = $this->t('Status');
    return $header + parent::buildHeader();
  }


  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    $row['label'] = $entity->label();
    $row['theme']['#markup'] = page_theme_get_theme_label($entity->getTheme());
    $row['status']['#markup'] = $entity->status() ? $this->t('Enabled') : $this->t('Disabled');
    return $row + parent::buildRow($entity);
  }


  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    drupal_set_message(t('The rule settings have been updated.'));
  }

}
