<?php

namespace Drupal\page_theme;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Form\FormStateInterface;


/**
 * Form controller for the page theme rule entity edit forms.
 */
class RuleForm extends EntityForm {


  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $entity = $this->entity;

    if (!$theme = $entity->getTheme()) {
      $theme = $this->config('system.theme')->get('default');
    }

    $form['label'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#default_value' => $entity->label(),
      '#maxlength' => 255,
      '#required' => TRUE,
    );
    $form['id'] = array(
      '#type' => 'machine_name',
      '#default_value' => $entity->id(),
      '#machine_name' => array(
        'exists' => '\Drupal\page_theme\Entity\Rule::load',
      ),
      '#maxlength' => EntityTypeInterface::BUNDLE_MAX_LENGTH,
      '#disabled' => !$entity->isNew(),
    );
    $form['theme'] = array(
      '#type' => 'select',
      '#title' => t('Theme'),
      '#default_value' => $theme,
      '#options' => page_theme_get_themes_list(),
    );

    $form['status'] = array(
      '#type' => 'checkbox',
      '#title' => $this->t('Enable rule'),
      '#default_value' => $entity->status(),
    );
    $form['weight'] = array(
      '#type' => 'weight',
      '#title' => $this->t('Weight'),
      '#description' => $this->t('Orders the page theme rules and the administrative listing. Heavier items will sink and the lighter items will be positioned nearer the top. Rules with equal weights are sorted alphabetically.'),
      '#default_value' => $entity->getWeight(),
    );

    return parent::form($form, $form_state, $entity);
  }


  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $entity = $this->entity;

    // Prevent leading and trailing spaces in rule names.
    $entity->set('label', trim($entity->label()));
    $status = $entity->save();

    if ($status == SAVED_UPDATED) {
      drupal_set_message($this->t('Rule %label has been updated.', array('%label' => $entity->label())));
      $this->logger('page_theme')->notice('Rule %label has been updated.', array('%label' => $entity->label(), 'link' => $this->entity->link($this->t('Edit'))));
    }
    else {
      drupal_set_message($this->t('Rule %label has been added.', array('%label' => $entity->label())));
      $this->logger('page_theme')->notice('Rule %label has been added.', array('%label' => $entity->label(), 'link' => $this->entity->link($this->t('Edit'))));
    }

    $form_state->setRedirect('entity.page_theme_rule.collection');
  }

}
