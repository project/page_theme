
Page Theme
==========

Allows to use different themes than the site default on specific pages.

Installation
------------

- Install this module using the official Drupal CMS instructions at
  (https://www.drupal.org/node/895232)

- Visit the configuration page under Administration > Appearance > Page theme
  (admin/appearance/page-theme)

License
-------

This project is GPL v2 software. See the LICENSE.txt file in this directory for
complete text.

Author
------

- Ralf Stamm (https://www.drupal.org/u/rstamm)

Support
-------

- https://www.drupal.org/project/issues/page_theme
